/*
Copyright © 2022 XPOL <paolofalomo@gmail.com>
License Restrictions
*/
package cmd

import (
	"altercode/xpol/utils"
	"fmt"
	"github.com/kolo/xmlrpc"
	"github.com/spf13/cobra"
	"strconv"
	"time"
)

var sendHome bool
var printers int
var fetchRate int64

type PronterFaceRPCStatusResponse struct {
	Temps struct {
		B []string `xmlrpc:"B"`
		T []string `xmlrpc:"T"`
	} `xmlrpc:"temps"`
	Progress     float32       `xmlrpc:"progress"`
	Estimation   []interface{} `xmlrpc:"eta"` //float64,float64,int --> secondsremain, secondsestimate, progress
	CurrentLayer any           `xmlrpc:"z"`
	FileName     string        `xmlrpc:"filename"`
}

// pronterCmd represents the pronter command
var pronterCmd = &cobra.Command{
	Use:     "pronter",
	Aliases: []string{"pronterface"},
	Short:   "Pronterface runner (printers manager)",
	Run: func(cmd *cobra.Command, args []string) {
		//REFERENCE https://github.com/kliment/Printrun/blob/master/printrun/rpc.py#L68sendhome

		if sendHome {
			/*err = pronterClient.Call("sendhome", nil, nil)
			if err != nil {
				utils.Err(err)
				return
			}*/
			//utils.Succ("Sending home")
			utils.Warn("Sending home is deprecated, will be moved to a specific command")
		} else {
			//EACH 10 seconds
			for true {
				fmt.Print("\033[H\033[2J")

				var rpcPort = 7978
				var printerNameSuffix = "Printer "
				//ITERATE over each printer on port's range
				for i := 0; i < printers; i++ {
					var printerName = printerNameSuffix + strconv.Itoa(rpcPort)
					pronterClient, err := xmlrpc.NewClient("http://localhost:"+strconv.Itoa(rpcPort), nil)
					if err != nil {
						utils.Warn(err.Error())
						//return
					}
					result := PronterFaceRPCStatusResponse{}

					err = pronterClient.Call("status", nil, &result)
					if err != nil {
						utils.Warn(err.Error())
						//return
					}
					//RPC STATUS CALL IS FINE, let see if hasura has a printer with this port
					printers, err := utils.FindPrinterByPort(strconv.Itoa(rpcPort))
					if err != nil {
						return
					}
					var printerUUID string
					//FIND OR INSERT a printer with a specified port
					if len(printers.XpolPrinter) > 0 {
						//FOUND a valid printer to update
						printerUUID = printers.XpolPrinter[0].ID
					} else {
						addedPrinter, err2 := utils.AddPrinterWithPort(strconv.Itoa(rpcPort), printerName)
						if err2 != nil {
							return
						}
						printerUUID = addedPrinter.InsertXpolPrinterOne.ID
					}

					//INIT FOR HASURA PAYLOAD
					printerStatus := utils.AddPrinterStatusVariables{
						PrinterId: "",
						File:      "",
						Layer:     utils.Numeric("0"),
						Progress:  utils.Numeric("0"),
						TempBed:   utils.Numeric("0"),
						TempTip:   utils.Numeric("0"),
					}
					//PRINTING STATUS SUMMARY
					utils.HeadLine(printerName)
					utils.SubHeadLine(printerUUID)
					for i, temperature := range result.Temps.B {
						utils.SuccKeyVal("Bed temperature sensor "+strconv.Itoa(i+1), temperature+"°C")
						if i == 0 {
							printerStatus.TempBed = utils.Numeric(temperature)
						}
					}
					for i, temperature := range result.Temps.T {
						utils.SuccKeyVal("Tip temperature sensor "+strconv.Itoa(i+1), temperature+"°C")
						if i == 0 {
							printerStatus.TempTip = utils.Numeric(temperature)
						}
					}
					utils.SuccKeyVal("Progress", strconv.FormatFloat(float64(result.Progress), 'f', 2, 64)+"/100")

					printerStatus.Progress = utils.Numeric(strconv.FormatFloat(float64(result.Progress), 'f', 2, 64))
					if len(result.Estimation) > 0 {
						utils.SuccKeyVal(
							"Estimation",
							strconv.FormatFloat(result.Estimation[0].(float64), 'f', 2, 64)+" seconds remaining of "+strconv.FormatFloat(result.Estimation[1].(float64), 'f', 2, 64)+" seconds estimated")
					}
					switch v := result.CurrentLayer.(type) {
					case int:
						utils.SuccKeyVal("Current layer", strconv.Itoa(v))
					case float32:
						utils.SuccKeyVal("Current layer", strconv.FormatFloat(float64(v), 'f', 2, 64))
						printerStatus.Layer = utils.Numeric(strconv.FormatFloat(float64(v), 'f', 2, 64))
					}
					if result.FileName != "" {
						utils.SuccKeyVal("File", result.FileName)
						printerStatus.File = utils.String(result.FileName)
					} else {
						utils.Warn("No files loaded")
					}
					//UPDATE HASURA FOR THIS PRINTER
					if printerUUID != "" {
						printerStatus.PrinterId = utils.UUID(printerUUID)
						statusUpdateResponse, err := utils.AddNewPrinterStatus(&printerStatus)
						if err != nil {
							utils.Err(err)
							return
						}
						if statusUpdateResponse.InsertXpolPrinterStatusOne.ID != "" {
							utils.Succ("HASURA updated")
						}
					}
					//INCREASE RPC PORT TO SCAN THE NEXT ONE
					rpcPort = rpcPort + 1
				}
				utils.Warn("Next update in " + strconv.Itoa(int(fetchRate)) + " seconds")
				time.Sleep(time.Second * time.Duration(fetchRate))

			}

		}

	},
}

func init() {
	rootCmd.AddCommand(pronterCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// pronterCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	pronterCmd.Flags().BoolVarP(&sendHome, "send-home", "", false, "Send Printers to home (deprecated)")
	pronterCmd.Flags().IntVarP(&printers, "printers", "n", 1, "Quantity of printers to fetch")
	pronterCmd.Flags().Int64VarP(&fetchRate, "fetch-rate", "u", 5, "Waiting time in seconds between each fetch. Suggested is 10")
}
