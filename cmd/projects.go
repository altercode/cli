/*
Copyright © 2022 XPOL <paolofalomo@gmail.com>
License Restrictions
*/
package cmd

import (
	"altercode/xpol/utils"
	"github.com/fatih/color"
	"github.com/spf13/cobra"
)

// projectsCmd represents the projects command
var projectsCmd = &cobra.Command{
	Use:   "projects",
	Short: "List available projects",
	Run: func(cmd *cobra.Command, args []string) {
		projects, err := utils.GetProjects()
		if err != nil {
			utils.Err(err)
		}
		if len(projects.XpolProject) > 0 {
			for _, prj := range projects.XpolProject {
				utils.Succ(prj.Name)
				cmd.Println("    " + prj.Description)
				cmd.Println("    " + "GitLab: " + color.New(color.Bold).Sprintf(prj.GitlabUrl))
				cmd.Println("")
			}
		}
	},
}

func init() {
	rootCmd.AddCommand(projectsCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// projectsCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// projectsCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
