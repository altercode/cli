/**
Copyright © 2022 XPOL <paolofalomo@gmail.com>
License Restrictions
*/
package cmd

import (
	"io/ioutil"
	"path/filepath"
	"strings"

	"github.com/fatih/color"
	"github.com/spf13/cobra"
	"gopkg.in/yaml.v2"
)

var LocalCliConfigFilePath = ".altercode.yml"

type AltercodeScriptRows []string

type AltercodeScript struct {
	Description string   `json:"description,omitempty"`
	Requires    []string `json:"requires,omitempty"`
	Powershell  bool     `json:"powershell,omitempty"`
	Script      AltercodeScriptRows
}
type AltercodeYAMLStructure struct {
	Scripts map[string]AltercodeScript
}

// runCmd represents the run command
var runCmd = &cobra.Command{
	Use:     "run script routine from the " + color.New(color.Italic).Sprintf(LocalCliConfigFilePath) + " file",
	Aliases: []string{"r"},
	Args:    cobra.ExactArgs(1),
	ValidArgsFunction: func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		config := getAltercodeYAML()
		keys := make([]string, 0, len(config.Scripts))
		for k := range config.Scripts {
			keys = append(keys, k)
		}
		if len(args) != 0 {
			return nil, cobra.ShellCompDirectiveNoFileComp
		}
		return keys, 0
	},
	Short: "run commands from " + color.New(color.Italic).Sprintf(LocalCliConfigFilePath) + " file",
	//DisableFlagParsing: true,
	Long: `This command will search for a ` + color.New(color.Italic).Sprintf(LocalCliConfigFilePath) + ` file and let you execute a script routine.
	Eg. rb run helloworld
	`,
	Run: func(cmd *cobra.Command, args []string) {
		cmd.SetOut(color.Output)
		if len(args) == 1 {
			runScriptByKey(args[0], cmd)
		}
	},
}

func runScriptByKey(scriptKey string, cmd *cobra.Command) {
	config := getAltercodeYAML()
	var selectedScript = config.Scripts[scriptKey]
	var runWithPowerShell = powershell
	if selectedScript.Powershell {
		runWithPowerShell = true
	}
	if len(selectedScript.Requires) > 0 {
		cmd.Println(color.New(color.BgHiGreen).Sprintf("REQUIRES: " + color.New(color.Bold).Sprintf(strings.Join(selectedScript.Requires, " and "))))

		for _, r := range selectedScript.Requires {
			runScriptByKey(r, cmd)
		}
	}
	for _, c := range selectedScript.Script {
		var commandDescription = selectedScript.Description
		cmd.Println(
			color.New(color.BgBlue).Sprintf(
				If(len(commandDescription) > 0,
					"SCRIPT: "+color.New(color.Bold).Sprintf(commandDescription),
					scriptKey,
				),
			),
		)
		cmd.Println(color.New(If(runWithPowerShell, color.BgMagenta, color.BgHiBlack)).Sprintf("RUNNING: " + color.New(color.Bold).Sprintf(c)))
		if runWithPowerShell {
			_, err := ExecutePowershell(c)
			if err != nil {
				return
			}
		} else {
			_, err := ExecuteCommand(c)
			if err != nil {
				return
			}
		}
	}
}

//TODO refactor this
func getAltercodeYAML() AltercodeYAMLStructure {
	var config AltercodeYAMLStructure
	configFilePathABS, _ := filepath.Abs(LocalCliConfigFilePath)
	configFileContent, errFile := ioutil.ReadFile(configFilePathABS)
	if errFile != nil {
		rootCmd.PrintErrln(errFile)
	}
	err := yaml.Unmarshal(configFileContent, &config)
	if err != nil {
		rootCmd.PrintErrln(err)
	}
	return config
}

var powershell = false

func init() {
	rootCmd.AddCommand(runCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// runCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	runCmd.Flags().BoolVarP(&powershell, "powershell", "p", false, "Run commands as powershell")
}
