/*
Copyright © 2022 XPOL <paolofalomo@gmail.com>
License Restrictions
*/
package cmd

import (
	"altercode/xpol/utils"
	"encoding/xml"
	"errors"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
	"path/filepath"

	"github.com/spf13/cobra"
)

// runInitCmd represents the runInit command
var runInitCmd = &cobra.Command{
	Use:   "run:init",
	Short: "Create " + LocalCliConfigFilePath + " if not exists",
	Run: func(cmd *cobra.Command, args []string) {
		CreateRBLabConfigFileIfNotExists()
	},
}

var AltercodeYAMLSchemaURL = "https://gitlab.com/altercode/cli/-/raw/main/altercode-yaml-schema.json"

var addSchemaToIdea = true

type SchemaOption struct {
	XMLName xml.Name         `xml:"option"`
	Name    string           `xml:"name,attr"`
	Value   string           `xml:"value,attr"`
	List    SchemaOptionList `xml:"list,omitempty"`
}
type SchemaOptionList struct {
	XMLName xml.Name `xml:"list,omitempty"`
	Items   []SchemaOptionListItem
}
type SchemaOptionListItem struct {
	XMLName xml.Name `xml:"Item"`
	Options []SchemaOption
}

type SchemaEntryMap struct {
	XMLName xml.Name `xml:"entry"`
	Key     string   `xml:"key,attr"`
	Value   SchemaEntryMapValue
}

type SchemaEntryMapValue struct {
	XMLName    xml.Name `xml:"value"`
	SchemaInfo SchemaEntryMapValueInfo
}
type SchemaEntryMapValueInfo struct {
	XMLName xml.Name `xml:"SchemaInfo"`
	Options []SchemaOption
}

type IdeaJsonSchema struct {
	XMLName   xml.Name `xml:"project"`
	Component struct {
		XMLName xml.Name `xml:"component"`
		Name    string   `xml:"name,attr"`
		State   struct {
			XMLName xml.Name `xml:"state"`
			Map     struct {
				XMLName xml.Name `xml:"map"`
				Entries []SchemaEntryMap
			}
		}
	}
}

func CreateRBLabConfigFileIfNotExists() {
	configFilePathABS, _ := filepath.Abs(LocalCliConfigFilePath)
	//ideaSchemaFilePathABS, _ := filepath.Abs(".idea/jsonSchemas.xml")

	if _, err := os.Stat(configFilePathABS); err == nil {
		utils.Warn(LocalCliConfigFilePath + " already exists")
	} else if errors.Is(err, os.ErrNotExist) {
		var m = new(AltercodeYAMLStructure)
		m.Scripts = make(map[string]AltercodeScript)
		m.Scripts["example"] = AltercodeScript{
			Description: "Example command Description",
			Script:      AltercodeScriptRows{"echo 'test'"},
		}
		content, err := yaml.Marshal(&m)
		if err != nil {
			utils.Err(err)
			return
		}
		err = ioutil.WriteFile(configFilePathABS, content, 0644)
		if err != nil {
			utils.Warn(err.Error())
		} else {
			utils.Succ(LocalCliConfigFilePath + " created")
			utils.Info("Schema can be found here " + AltercodeYAMLSchemaURL)
		}
		/*if addSchemaToIdea {
			ideaSchemaFile, err := ioutil.ReadFile(ideaSchemaFilePathABS)
			ideaSchema := new(IdeaJsonSchema)
			if err != nil {
				utils.Warn(err.Error())
			} else {
				err = xml.Unmarshal(ideaSchemaFile, ideaSchema)
				if err != nil {
					return
				}
			}
			shouldAddMapping := true
			for _, schemaMapping := range ideaSchema.Component.State.Map.Entries {
				if schemaMapping.Key == "altercode-yaml-schema" {
					shouldAddMapping = false
					break
				}
			}
			if shouldAddMapping {
				entryMap := SchemaEntryMap{
					Key: "altercode-yaml-schema",
					Value: SchemaEntryMapValue{
						SchemaInfo: SchemaEntryMapValueInfo{
							Options: []SchemaOption{
								{
									Name:  "generatedName",
									Value: "Altercode Schema mapping",
								},
								{
									Name:  "name",
									Value: "altercode-yaml-schema",
								},
								{
									Name:  "relativePathToSchema",
									Value: AltercodeYAMLSchemaURL,
								},
								{
									Name: "patterns",
									List: SchemaOptionList{
										Items: []SchemaOptionListItem{
											{
												Options: []SchemaOption{
													{
														Name: "path", Value: ".altercode.yml",
													},
												},
											},
										},
									},
								},
							},
						},
					},
				}

				ideaSchema.Component.State.Map.Entries = append(ideaSchema.Component.State.Map.Entries, entryMap)
				xmlFile, _ := os.OpenFile(ideaSchemaFilePathABS, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600)
				_, err := xmlFile.WriteString(xml.Header)
				if err != nil {
					utils.Warn(err.Error())
					return
				}
				encoder := xml.NewEncoder(xmlFile)
				defer xmlFile.Close()
				err = encoder.Encode(&ideaSchema)
				encoder.Indent("", "\t")
				if err != nil {
					utils.Warn(err.Error())
				} else {
					utils.Succ(ideaSchemaFilePathABS + " updated")
				}
			}

			//fmt.Printf("v = %#v\n", ideaSchema)
			fmt.Printf("v = %#v\n", ideaSchema.Component.Name)

			utils.Succ("Added yaml schema to idea")

		}*/
	}

}

func init() {
	rootCmd.AddCommand(runInitCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// runInitCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	runInitCmd.Flags().BoolVarP(&addSchemaToIdea, "no-idea-schema", "n", true, "Ignore adding schema to idea")
}
