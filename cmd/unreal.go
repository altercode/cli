/*
Copyright © 2022 XPOL <paolofalomo@gmail.com>
License Restrictions
*/
package cmd

import (
	"altercode/xpol/utils"
	"errors"
	"fmt"
	"github.com/schollz/progressbar/v3"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

var noInteractionBuild bool
var ignoreIdentical bool
var dryRun bool
var sourceProjectDir string
var destProjectDir string
var proceed = false
var filesCount int64 = 0
var progessBar progressbar.ProgressBar

// unrealCmd represents the unreal command
var unrealCmd = &cobra.Command{
	Use:     "unreal",
	Aliases: []string{"u"},
	Short:   "Unreal Engine Utility Commands",
	Run: func(cmd *cobra.Command, args []string) {
		err := cmd.Help()
		if err != nil {
			return
		}
	},
}

// unrealBuildCmd represents the unreal command
var unrealBuildCmd = &cobra.Command{
	Use:   "build",
	Short: "Run RunUAT.bat file easly",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		uProjectFile, _ := filepath.Abs(args[0])
		_, errFile := ioutil.ReadFile(uProjectFile)
		if errFile != nil {
			rootCmd.PrintErrln(errFile)
			return
		}
		uProjectDir := filepath.Dir(uProjectFile)
		UEPath := viper.GetString("uepath")
		RunUAT := UEPath + "\\Engine\\Build\\BatchFiles\\RunUAT.bat"
		_, errFileRunUAT := ioutil.ReadFile(RunUAT)
		if errFileRunUAT != nil {
			rootCmd.PrintErrln(errFileRunUAT)
			return
		}
		buildCommand := RunUAT + " BuildCookRun -utf8output -platform=Win64 -clientconfig=Development -serverconfig=Development -project=" + uProjectFile + " -noP4 -nodebuginfo -allmaps -cook -build -stage -prereqs -pak -archive -archivedirectory=" + filepath.Join(uProjectDir, "Builds")
		if !noInteractionBuild {
			utils.Info(buildCommand)
			proceed := YesNo(YesNoParameters{YesNoLabel: "Do you want to proceed?"})
			if !proceed {
				return
			}
		}
		_, _ = ExecutePowershell(buildCommand)
	},
}

// unrealSetUePathCmd represents the unreal command
var unrealSetUePathCmd = &cobra.Command{
	Use:   "set-path",
	Short: "Set path of unreal engine installation",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		err := viperSetSave("uepath", args[0])
		if err != nil {
			return
		}
	},
}

// unrealGetUePathCmd represents the unreal command
var unrealGetUePathCmd = &cobra.Command{
	Use:   "get-path",
	Short: "Get path of unreal engine installation",
	Run: func(cmd *cobra.Command, args []string) {
		path := viper.GetString("uepath")
		if path != "" {
			utils.Succ(path)
		} else {
			utils.Warn("UE path has not been set yet")
		}
	},
}

var unrealMigrateCmd = &cobra.Command{
	Use:   "migrate [source .uproject] [destination .uproject] [path to migrate]",
	Short: "Migrate a specific path from one Unreal Engine project to another",
	Args:  cobra.ExactArgs(3),
	RunE: func(cmd *cobra.Command, args []string) error {
		sourceUProjectFile := args[0]
		destUProjectFile := args[1]
		pathToMigrate := args[2]

		// Check if the source .uproject file exists
		if _, err := os.Stat(sourceUProjectFile); os.IsNotExist(err) {
			return fmt.Errorf("the source .uproject file '%s' does not exist", sourceUProjectFile)
		}

		// Check if the destination .uproject file exists
		if _, err := os.Stat(destUProjectFile); os.IsNotExist(err) {
			return fmt.Errorf("the destination .uproject file '%s' does not exist", destUProjectFile)
		}

		// Migrate the path from the source project to the destination project
		err := migratePathBetweenProjects(sourceUProjectFile, destUProjectFile, pathToMigrate)
		if err != nil {
			return err
		}

		if !dryRun {
			if proceed {
				utils.Succ("Migration completed successfully!")
			}
		}
		return nil
	},
}

func migratePathBetweenProjects(sourceUProjectFile, destUProjectFile, pathToMigrate string) error {
	// Get the directories of the source and destination projects
	sourceProjectDir = filepath.Dir(sourceUProjectFile)
	destProjectDir = filepath.Dir(destUProjectFile)

	// Construct the full paths for the source and destination
	sourcePath := filepath.Join(sourceProjectDir, "Content", pathToMigrate)
	destPath := filepath.Join(destProjectDir, "Content", pathToMigrate)

	utils.HeadLine("Unreal Engine Altercode Migration tool")
	utils.SubHeadLine("Overview")
	utils.KeyVal("Source", sourcePath)
	utils.KeyVal("Destination", destPath)

	// Copy the files/folders from the source path to the destination path
	if dryRun {
		err := copyPath(sourcePath, destPath, true)
		if err != nil {
			utils.Err(err)
			return err
		}
		return nil
	} else {
		_ = copyPath(sourcePath, destPath, true)
		proceed = YesNo(YesNoParameters{YesNoLabel: "Do you want to proceed?"})
		if !proceed {
			utils.Warn("Aborted")
			return nil
		}
		progessBar = *progressbar.Default(filesCount)
	}
	err := copyPath(sourcePath, destPath, false)
	if err != nil {
		return fmt.Errorf("failed to migrate path '%s': %v", pathToMigrate, err)
	}

	return nil
}

func copyPath(src, dst string, dryRun bool) error {

	// Create the destination directory if it doesn't exist
	if !dryRun {
		err := os.MkdirAll(dst, os.ModePerm)
		if err != nil {
			return err
		}
	} else {
		if Verbose {
			utils.SubHeadLine(strings.Replace(src, sourceProjectDir, "", 1))
		}
		_, readDestDirErr := os.ReadDir(dst)
		if readDestDirErr != nil {
			utils.Italic("Would " + utils.GreenString("create directory") + ": " + dst)
		} else {
			utils.Italic("Would " + utils.YellowString("update directory") + ": " + dst)
		}
	}

	// Get a list of files and directories in the source path
	srcFiles, err := os.ReadDir(src)
	if err != nil {
		return err
	}

	// Get a list of files and directories in the destination path
	dstFiles, err := os.ReadDir(dst)
	safe := true
	if err != nil {
		if !dryRun {
			return err
		} else {
			safe = false
		}
	}

	if safe {
		// Remove files and directories from the destination that are not present in the source
		for _, file := range dstFiles {
			dstPath := filepath.Join(dst, file.Name())
			srcPath := filepath.Join(src, file.Name())

			if _, err := os.Stat(srcPath); os.IsNotExist(err) {
				// The file/directory is not present in the source, remove it from the destination
				if !dryRun {
					if file.IsDir() {
						err = os.RemoveAll(dstPath)
					} else {
						err = os.Remove(dstPath)
					}
					if err != nil {
						return err
					}
				} else {
					if file.IsDir() {
						utils.Italic("Would " + utils.RedString("remove") + " directory:" + dstPath)
					} else {
						utils.Italic("Would " + utils.RedString("remove") + " file: " + dstPath)
					}
				}
			}
		}
	}

	// Copy files and directories from the source to the destination
	for _, file := range srcFiles {
		srcPath := filepath.Join(src, file.Name())
		dstPath := filepath.Join(dst, file.Name())

		if file.IsDir() {
			// Recursively copy the directory
			err = copyPath(srcPath, dstPath, dryRun)
			if err != nil {
				return err
			}
		} else {
			// Copy the file
			err = copyFile(srcPath, dstPath, dryRun)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func copyFile(src, dst string, dryRun bool) error {
	// Open the source file
	srcFile, err := os.Open(src)
	if err != nil {
		return err
	}
	defer srcFile.Close()

	// Create the destination file
	if !dryRun {
		dstFile, err := os.Create(dst)
		if err != nil {
			return err
		}
		defer dstFile.Close()

		// Copy the contents of the source file to the destination file
		_ = progessBar.Add(1)
		_, err = io.Copy(dstFile, srcFile)
		if err != nil {
			return err
		}
	} else {
		filesCount++
		if Verbose {
			if dstFile, err := os.Stat(dst); err == nil {
				if srcFileStat, err := os.Stat(src); err == nil {
					if srcFileStat.Size() == dstFile.Size() {
						if !ignoreIdentical {
							utils.Bold(" Ͱ " + utils.BlueString("write idential file") + ": " + filepath.Base(src))
							return nil
						}
					}
				}
				utils.Bold(" Ͱ " + utils.YellowString("update") + ": " + filepath.Base(src))
			} else if errors.Is(err, os.ErrNotExist) {
				utils.Bold(" Ͱ " + utils.GreenString("copy") + ": " + filepath.Base(src))
			}
		}
	}

	return nil
}
func init() {
	rootCmd.AddCommand(unrealCmd)
	unrealCmd.AddCommand(unrealBuildCmd)
	unrealCmd.AddCommand(unrealSetUePathCmd)
	unrealCmd.AddCommand(unrealGetUePathCmd)
	unrealMigrateCmd.Flags().BoolVarP(&dryRun, "dry-run", "d", false, "Dry RUN the migration, do not do the process")
	unrealMigrateCmd.Flags().BoolVarP(&ignoreIdentical, "ignore-identical", "g", false, "Ignore to show identical files")
	unrealCmd.AddCommand(unrealMigrateCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// unrealCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	unrealBuildCmd.Flags().BoolVarP(&noInteractionBuild, "non-interactive", "y", false, "Ask before execution")
}
