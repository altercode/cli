/*
Copyright © 2022 XPOL <paolofalomo@gmail.com>
License Restrictions
*/
package cmd

import (
	"github.com/spf13/cobra"
)

var XPOLWebURL = "https://xpol.paolofalomo.it/"

// webCmd represents the web command
var webCmd = &cobra.Command{
	Use:   "web",
	Short: "Opens the XPOL Web interface in browser",
	Run: func(cmd *cobra.Command, args []string) {
		OpenBrowserUrl(XPOLWebURL)
	},
}

func init() {
	rootCmd.AddCommand(webCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// webCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// webCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
