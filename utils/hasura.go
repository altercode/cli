package utils

import "net/http"

var HasuraGraphQlServerURL = "https://hasura.altercode.dev/v1/graphql"

func HasuraGraphQLClient(public ...bool) *Client {
	if IsLoggedIn() {
		return &Client{
			Client: &http.Client{},
			Url:    HasuraGraphQlServerURL,
		}
	}
	if len(public) == 0 {
		Warn("You are not logged in")
	}
	return &Client{
		Client: &http.Client{},
		Url:    HasuraGraphQlServerURL,
	}
}
