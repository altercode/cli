package utils

import (
	"encoding/json"
	"github.com/golang-jwt/jwt/v4"
	"github.com/manifoldco/promptui"
	"golang.org/x/crypto/bcrypt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

var hmacSecret = []byte("0smBoJ7ouIcj9HRxmoo8EGf3D7pXLWIL")

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

func GetUserByName(name string) (*FindUserResponse, error) {
	req, err := NewFindUserRequest(HasuraGraphQlServerURL, &FindUserVariables{Name: String(name)})
	if err != nil {
		return nil, err
	}
	return req.Execute(HasuraGraphQLClient(true).Client)
}

type LoginUser struct {
	Name     string `json:"name"`
	Password string `json:"password"`
	ID       string `json:"id"`
}

func ValidateLogin(name string, pass string) (bool, LoginUser, error) {
	user, err := GetUserByName(name)
	if err != nil {
		return false, LoginUser{}, err
	}
	if len(user.XpolUser) > 0 {
		var firstUser = user.XpolUser[0]
		if CheckPasswordHash(pass, firstUser.Password) {
			return true, firstUser, nil
		}
		Warn("Password invalid")
		return false, firstUser, nil

	} else {
		Warn("User " + name + " not found")
	}
	return false, LoginUser{}, nil
}

func GenerateJWT(user LoginUser) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"sub": "1",
		"https://hasura.io/jwt/claims": map[string]interface{}{
			"x-hasura-allowed-roles": []string{"admin", "xpol"},
			"x-hasura-default-role":  "xpol",
			"x-hasura-user-id":       "1",
		},
	})
	// Sign and get the complete encoded token as a string using the secret
	tokenString, err := token.SignedString(hmacSecret)
	return tokenString, err
}

type XPOLJWTJson struct {
	JWT string `json:"jwt"`
}

var jwtPath = XPOLDirFile("xpol-jwt.json")

func StoreJWT(jwtString string) {
	f, err := os.OpenFile(jwtPath, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		log.Fatalf("Unable to save xpol-jwt.json: %v", err)
	}
	defer func(f *os.File) {
		err2 := f.Close()
		if err2 != nil {
			//cmd.Err(err2)
		}
	}(f)
	err = json.NewEncoder(f).Encode(&XPOLJWTJson{
		JWT: jwtString,
	})
	if err != nil {
		Err(err)
		return
	}
}

func PromptLogin(label string, hidden bool) string {
	promptKey := promptui.Prompt{
		Label:       label,
		HideEntered: hidden,
	}
	if hidden {
		promptKey.Mask = '*'
	}
	promptReturn, err := promptKey.Run()

	if err != nil {
		Err(err)
		return ""
	}
	return promptReturn
}

func LogOut() {
	StoreJWT("")
}

func IsLoggedIn() bool {
	return GetJWT() != ""
}

func GetJWT() string {
	b, err := ioutil.ReadFile(jwtPath)
	if err != nil {
		return ""
	}
	var JWTJsonFile XPOLJWTJson
	err = json.Unmarshal(b, &JWTJsonFile)
	if err != nil {
		return ""
	}
	return JWTJsonFile.JWT
}

func GetRequestWithAuth(req *http.Request) *http.Request {
	bearer := GetJWT()
	if bearer != "" {
		req.Header.Set("Authorization", "Bearer "+bearer)
		req.Header.Set("x-hasura-role", "xpol")
	}
	return req
}
