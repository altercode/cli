package utils

//GENERAL UTILS
import (
	"fmt"
	"github.com/fatih/color"
	"github.com/mitchellh/go-homedir"
	"os"
)

func XPOLDir() string {
	homeDir, _ := homedir.Dir()
	xpolDir := homeDir + "/.xpol"
	_ = os.MkdirAll(xpolDir, os.ModePerm)
	return xpolDir
}

func XPOLDirFile(path string) string {
	return XPOLDir() + "/" + path
}

//COLORING

func RedString(s string) string {
	return color.New(color.FgRed).Sprint(s)
}
func GreenString(s string) string {
	return color.New(color.FgGreen).Sprint(s)
}
func YellowString(s string) string {
	return color.New(color.FgYellow).Sprint(s)
}
func BlueString(s string) string {
	return color.New(color.FgBlue).Sprint(s)
}

//PRINTING

func HeadLine(line string) {
	fmt.Println(color.New(color.Bold).Sprintf("===== " + line + " ====="))
}
func SubHeadLine(line string) {
	fmt.Println(color.New(color.Bold).Sprintf("---- " + line + " ----"))
}
func Succ(line string) {
	fmt.Println(GreenString(line))
}
func Info(line string) {
	fmt.Println(BlueString(line))
}
func Italic(line string) {
	fmt.Println(color.New(color.Italic).Sprintf(line))
}
func Bold(line string) {
	fmt.Println(color.New(color.Bold).Sprintf(line))
}
func KeyVal(key string, value string) {
	fmt.Println(key + ": " + color.New(color.Bold).Sprintf(value))
}
func SuccKeyVal(key string, value string) {
	KeyVal(key, GreenString(value))
}
func Warn(line string) {
	fmt.Println(YellowString(line))
}
func Err(err error) {
	fmt.Println(RedString(err.Error()))
}
