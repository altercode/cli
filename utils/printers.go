package utils

func FindPrinterByPort(port string) (*GetFirstPrinterByPortResponse, error) {
	req, err := NewGetFirstPrinterByPortRequest(HasuraGraphQlServerURL, &GetFirstPrinterByPortVariables{PrinterPort: String(port)})
	if err != nil {
		return nil, err
	}

	req.Request = GetRequestWithAuth(req.Request)

	return req.Execute(HasuraGraphQLClient().Client)
}

func AddPrinterWithPort(port string, name string) (*AddPrinterResponse, error) {
	req, err := NewAddPrinterRequest(HasuraGraphQlServerURL, &AddPrinterVariables{
		Name:  String(name),
		Port:  String(port),
		State: nil,
	})
	if err != nil {
		return nil, err
	}

	req.Request = GetRequestWithAuth(req.Request)

	return req.Execute(HasuraGraphQLClient().Client)
}

func AddNewPrinterStatus(printerStatus *AddPrinterStatusVariables) (*AddPrinterStatusResponse, error) {
	req, err := NewAddPrinterStatusRequest(HasuraGraphQlServerURL, printerStatus)
	if err != nil {
		return nil, err
	}

	req.Request = GetRequestWithAuth(req.Request)

	return req.Execute(HasuraGraphQLClient().Client)
}
